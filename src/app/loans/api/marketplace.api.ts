import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Market} from '../models/Market';

@Injectable()
export class MarketplaceApi {

  readonly API = '/loans/marketplace';
  readonly TOTAL_RESULTS = 100;

  constructor(private http: HttpClient) {}

  /**
   * Vrati zaznamy ruzneho typu
   */
  getMarketplaces(): Observable<Market[]> {
    return this.http.get<Market[]>(this.API);
  }

  /**
   * Najde poslednich 100 zaznamu podle typu ratingu
   * @param search String; typ ratingu
   */
  searchMarketplaces(search:string): Observable<Market[]>{
    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    headers = headers.append("content-type", "application/json");
    headers = headers.append("Access-Control-Allow-Origin", "*");
    headers = headers.append('X-Total', this.TOTAL_RESULTS.toString());

    return this.http.get<Market[]>(`${this.API}/?rating__eq=${search}`,{ headers: headers});
  }


}
