import {Component, OnInit, Output} from '@angular/core';
import {MarketplaceFacade} from '../../marketplace.facade';
import {Observable} from 'rxjs';
import {SelectMarketplace} from '../../models/SelectMarketplace';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.scss']
})
export class MarketplaceComponent implements OnInit {

  @Output() public averageMarket: string;
  @Output() public selectizeMarketplace: SelectMarketplace[];
  @Output() public breakpoint: number;
  @Output() public isUpdating: boolean;
  @Output() public selectMarketValue: SelectMarketplace;

  private averagePrice$: Observable<number>;
  private isUpdating$: Observable<boolean>;

  constructor(private marketplaceFacade: MarketplaceFacade, private route: ActivatedRoute) {
    this.averagePrice$ = marketplaceFacade.getAveragePriceOfMarket$();
    this.isUpdating$ = marketplaceFacade.isUpdating$();

    this.selectizeMarketplace = this.marketplaceFacade.selectizeMarketplace();

    this.route.params.subscribe(params => {
      let selectMarketplace = params['selectMarketplace'];
      this.selectMarketValue = this.marketplaceFacade.getSelectMarketplaceValue(selectMarketplace);
      this.averagePriceMarketplace(selectMarketplace);
    });
  }

  ngOnInit() {
    this.averagePrice$.subscribe((averagePriceMarket: number) => {
      this.averageMarket = this.formatingCurrency(averagePriceMarket);
    });

    this.isUpdating$.subscribe((isUpdating: boolean) => {
      this.isUpdating = isUpdating;
    });

    this.breakpoint = (window.innerWidth <= 700) ? 6 : this.selectizeMarketplace.length;
  }

  averagePriceMarketplace(selectMarketplace: string) {
    this.marketplaceFacade.averagePriceMarketplace(selectMarketplace);
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 700) ? 6 : 12;
  }

  formatingCurrency(averagePriceMarket: number): string {
    if(averagePriceMarket == null) {
      return null;
    }

    return new Intl.NumberFormat('cs-CZ', {
      style: 'currency',
      currency: 'CZK',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }).format(averagePriceMarket);
  }

}
