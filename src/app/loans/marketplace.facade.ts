import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MarketplaceApi} from './api/marketplace.api';
import {map, shareReplay, tap} from 'rxjs/operators';
import {Market} from './models/Market';
import {SelectMarketplace} from './models/SelectMarketplace';
import {MarketplaceState} from './state/Marketplace.state';

@Injectable()
export class MarketplaceFacade {

  constructor(private marketplaceApi: MarketplaceApi, private marketplaceState: MarketplaceState) {
  }

  isUpdating$(): Observable<boolean> {
    return this.marketplaceState.isUpdating$();
  }

  getMarketplaces$(): Observable<Market[]> {
    return this.marketplaceApi.getMarketplaces().pipe(tap(marketplaces => this.marketplaceState.setMarketplaces(marketplaces)));
  }

  getAveragePriceOfMarket$(): Observable<number> {
    return this.marketplaceState.getAveragePriceOfMarket$();
  }

  getSelectMarketplaceValue(selectMarketplace: string): SelectMarketplace {
    return this.marketplaceState.getSelectMarketplaceValue(selectMarketplace);
  }

  averagePriceMarketplace(search: string) {
    this.marketplaceState.setUpdating(true);
    this.marketplaceApi.searchMarketplaces(search).subscribe(
      (markets: Market[]) => {
        this.marketplaceState.averagePriceMarketplace(markets);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  selectizeMarketplace(): SelectMarketplace[] {
    return this.marketplaceState.selectizeMarketplace();
  }

}
