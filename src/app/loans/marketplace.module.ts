import {NgModule} from '@angular/core';

import {HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {GestureConfig} from '@angular/material';
import {MarketplaceComponent} from './components/marketplace/Marketplace.component';
import {MarketplaceFacade} from './marketplace.facade';
import {MarketplaceApi} from './api/marketplace.api';
import {MaterialModule} from '../layout/material/material.module';
import {RouterModule} from '@angular/router';
import {MarketplaceState} from './state/Marketplace.state';

@NgModule({
  exports: [],
  declarations: [
    MarketplaceComponent
  ],
  imports: [
    MaterialModule,
    RouterModule
  ],
  providers: [
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig},
    MarketplaceFacade,
    MarketplaceApi,
    MarketplaceState
  ]
})
export class MarketplaceModule {
}
