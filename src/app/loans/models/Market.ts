import {Photos} from './Photos';

export class Market {

  id: number;
  url: string;
  name: string;
  story: string;
  purpose: string;
  photos: Photos[];
  userId: number;
  nickName: string;
  termInMonths: number;
  interestRate: number;
  revenueRate: number;
  annuity: number;
  premium: {};
  rating: string;
  topped: boolean;
  amount: number;
  remainingInvestment: number;
  investmentRate: number;
  covered: boolean;
  reservedAmount: number;
  zonkyPlusAmount: number;
  datePublished: string;
  published: boolean;
  deadline: string;
  myOtherInvestments: {};
  borrowerRelatedInvestmentInfo: {};
  investmentsCount: number;
  questionsCount: number;
  region: string;
  mainIncomeType: string;
  questionsAllowed: boolean;
  activeLoansCount: number;
  insuranceActive: boolean;
  insuranceHistory: any[];
  fastcash: boolean;
  multicash: boolean;
  currency: string;
  insuredInFuture: boolean;
  annuityWithInsurance: number;

  constructor() {
  }
}
