import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Market} from '../models/Market';
import {SelectMarketplace} from '../models/SelectMarketplace';

@Injectable()
export class MarketplaceState {

  private updating$ = new BehaviorSubject<boolean>(false);
  private marketplaces$ = new BehaviorSubject<Market[]>(null);
  private averagePrice$ = new BehaviorSubject<number>(null);

  isUpdating$() {
    return this.updating$.asObservable();
  }

  setUpdating(isUpdating: boolean) {
    this.updating$.next(isUpdating);
  }

  getMarketplaces$() {
    return this.marketplaces$.asObservable();
  }

  getAveragePriceOfMarket$() {
    return this.averagePrice$.asObservable();
  }

  setMarketplaces(marketplaces: Market[]) {
    this.marketplaces$.next(marketplaces);
  }

  /**
   * Zprumeruje vysi pujcek
   * @param marketplaces: Market[]
   */
  averagePriceMarketplace(marketplaces: Market[]) {
    let amountPrice: number = 0, averagePrice: number;

    if (marketplaces.length > 0) {
      for (let marketplace of marketplaces) {
        amountPrice += marketplace.amount;
      }
      averagePrice = amountPrice / marketplaces.length;
    }

    this.averagePrice$.next(averagePrice);
    this.setUpdating(false);
  }

  /**
   * Vrati vsechny typy ratingu
   */
  selectizeMarketplace(): SelectMarketplace[] {
    return [
      {key: 'AAAAAA', value: '2.99%'},
      {key: 'AAAAA', value: '3.99%'},
      {key: 'AAAA', value: '4.99%'},
      {key: 'AAA', value: '5.99%'},
      {key: 'AAE', value: '6.99%'},
      {key: 'AA', value: '8.49%'},
      {key: 'AE', value: '9.49%'},
      {key: 'A', value: '10.99%'},
      {key: 'B', value: '13.49%'},
      {key: 'C', value: '15.49%'},
      {key: 'D', value: '19.99%'}
    ];

  }


  /**
   * Vyselektuje rating podle klice
   * @param selectMarketplace
   */
  getSelectMarketplaceValue(selectMarketplace: string): SelectMarketplace {
    let markets = this.selectizeMarketplace();
    let elementPos = markets.map(function (x: SelectMarketplace) {
      return x.key;
    }).indexOf(selectMarketplace);
    return markets[elementPos];
  }


}
